﻿Imports AbstractFactory

Module Module1

    Sub Main()
        Dim fabrica As Pizzeria
        fabrica = New PizzeriaArgentina()
        Dim pizza As Pizza = fabrica.CrearPizza()

        Console.WriteLine(pizza.Descripcion)

        fabrica = New PizzeriaItaliana()
        pizza = fabrica.CrearPizza()
        Console.WriteLine(pizza.Descripcion)

        Console.ReadKey()
    End Sub

End Module
